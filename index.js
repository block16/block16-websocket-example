$(function() {
  const web3 = new Web3(new Web3.providers.HttpProvider('https://mainnet.infura.io/'));
  const connection = new WebSocket('wss:api.block16.io/v1/block/current/ws');
  const tokenTransferHash = web3.utils.sha3('Transfer(address,address,uint256)');
  const symbolFn = web3.eth.abi.encodeFunctionSignature('symbol()');
  const decimalsFn = web3.eth.abi.encodeFunctionSignature('decimals()');

  Date.prototype.formatShort = function() {
    return (this.getHours() >= 10 ? this.getHours() : ('0' + this.getHours())) +
      ':' + (this.getMinutes() >= 10 ? this.getMinutes() : ('0' + this.getMinutes())) +
      ' ' + (this.getMonth() + 1) +
      '/' +  this.getDate() +
      '/' +  this.getFullYear();
  };

  const appendToTable = (type, from, to, value, time) => {
    const tableElement = '<tr>' +
      '<td>' + type + '</td>' +
      '<td>' + from + '</td>' +
      '<td>' + to + '</td>' +
      '<td>' + value + '</td>' +
      '<td>' + time + '</td>' +
      '</tr>';

    $('#transaction-table tr:first').after(tableElement);
  };

  connection.onmessage = (message) => {
    let msgData = JSON.parse(message.data);

    let time = new Date(web3.utils.hexToNumber(msgData.timestamp * 1000)).formatShort();
    for (let i = 0; i < msgData.transactions.length; i++) {
      let tx = msgData.transactions[i];
      let receipt = msgData.receipts[i];
      let addEthTx = true;
      // Parse the logs for token transactions
      for(let j = 0; j < receipt.logs.length; j++) {
        let log = receipt.logs[j];
        if (log.topics.length === 3 && log.topics[0] === tokenTransferHash) {
          addEthTx = false;
          web3.eth.call({
            to: tx.to,
            data: symbolFn
          }).then((name) => {
            const symbol = web3.utils.hexToAscii(name).replace(/[^\x20-\x7F]/g, "").trim();
            if (symbol !== '') {
              web3.eth.call({to: tx.to, data: decimalsFn}).then(contractDecimals => {
                const decimalsBN = new BigNumber(contractDecimals.substring(2), 16).toNumber();
                const from = '0x' + log.topics[1].substring(log.topics[1].length - 40, log.topics[1].length);
                const to = '0x' + log.topics[2].substring(log.topics[2].length - 40, log.topics[2].length);
                const value = new BigNumber(log.data, 16).div(decimalsBN);
                if (value.isFinite()) {
                  appendToTable(symbol, from, to, value, time);
                }
              });
            }
          });
        }
      }

      // Only add the ETH tx if it didn't contain token transfers
      if (addEthTx) {
        let type = 'ETH';
        let from = tx.from;
        let to = tx.to;
        let value = web3.utils.fromWei(web3.utils.hexToNumberString(tx.value), 'ether');
        appendToTable(type, from, to, value, time);
      }
    }
  };
});